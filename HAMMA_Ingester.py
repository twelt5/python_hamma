# -*- coding: utf-8 -*-
"""
Created on Tue Nov  7 22:59:24 2017

@author: Tyler
"""

#HAMMA Data Importer
#	By: Tyler Welty
#	Date: 7 November 2017
#		Mentored by: Dr. Phillip Bitzer
#		Funded by: The University of Alabama in Huntsville
#				   Atmospheric Science Department and ESSC
#
#  Changelog:
#    29 November 2017 - Created
#    29 November 2017 - Binary data ingester, header, and data functions written
#    29 November 2017 - Accounted for filter data and left in some TODOs
#    29 November 2017 - Added in code documentation
#    30 Novmeber 2017 - Added in plotting routine and options for easy visualization of hamma data
#    30 November 2017 - Experimented with some less memory intensive methods... still very memory heavy
#    01 December 2017 - Added Routine, Example, and Other Major Sets of Documentation
#    13 December 2017 - Added Routine for finding offset and standard deviation of a given data set
#    18 December 2017 - Added translator for HAMMA 2 headers, changed version check to a sub-routine
#    18 December 2017 - Fixed a bug in the hamma_offset routing (error in finding standard deviation fixed)


"""File 
and 
Routine 
Documentation"""

#Import Necessary Libraries/Header Files
#import sys
import struct #Allows you to use structures

from matplotlib import pyplot as plt #imports plotting routines
import matplotlib

#Allow for math functions
import math


#Define Function
def hamma_ingester(file):
    "Function reads in a binary HAMMA file and returns its contents to the user - Note: Using L1 data only"
    #Read in Entire File
    SensorFile = open(file, 'rb') #open file as read, binary - returns file object SensorFile
    FileContents = SensorFile.read() #Read in entire file
    SensorFile.seek(0) #Return to start of file
    
    #Determine File Length and Store as an Integer
    FileLength = int(len(FileContents))
    
    #Based on File Length, Determine if a Filter is Present
    #  The Filter Information is an 18 byte array that follows the header.
    #  With no filter, a full file will have length 100005000.
    #    This is 50 2000000 byte data sections and 50 100 byte headers.
    #  With a filter, a full file will have an additional 900 bytes (50 times 18)
    #  So a no-filter file will be some multiple of 2000100 bytes,
    #  and a filter file will have some multiple of 2000118 bytes.
    if((FileLength%2000100) == 0):
        FilterPresent = 0 #Set FilterPresent to 0 if the file is a multiple of bytes that indicates no filter
    elif((FileLength%2000118) == 0):
        FilterPresent = 1 #Set FitlerPresent to 1 if the file is a multiple of bytes that does indicate a filter
    else:
        #If the file length is not a multiple of processed data with or without a filter, then throw "error"
        print('The file length was different from expected... Make sure to use proper L1 HAMMA data')
        return
    
    #Determine Sensor Version
    #  This is based on the sync bytes of the sensor;
    #  Since dealing with L1 data, the first 8 bytes will always be sync bytes.
    #  If first byte is F2, or 242, then it is HAMMA 1.5 Data
    #***TODO*** -> Add in other HAMMA versions
    #  Read in First Sync Byte
    SyncByteArr = struct.unpack("B", SensorFile.read(1)) #Returns single item tuple containing sync byte value as its first element
    SyncByte = SyncByteArr[0] #Takes the value of the first sync byte out of the tuple and makes it a simple integer
    #With value of first sync byte, can now check version:
    #***TODO*** -> If the first byte isn't enough to determine version, need to adjust so that it reads in all 4 sync bytes
    #              and stores them as an array whose values can be checked
    #***TODO*** -> Add other HAMMA versions
    """if(SyncByte == 242): #242 = 'F2'
        #Then this is HAMMA 1.5 and we'll use that header definition, stored in a seperate helper function for clarity
        Version = 'HAMMA_15'
    else:
        #If not a recognized version, throw "error"
        print('The file does not seem to be a HAMMA version this program can handle...')
        return"""
    #Changed from in program check to a seperate check function
    Version = hamma_version_check(SyncByte)
    
    #Return to the Start of File for Good Measure
    SensorFile.seek(0)
    
    #Break File into its Headers and Data
    #  Store headers and data in lists
    #  So make a dictionary for each header, then store those dictionaries in a list of header dictionaries
    #  For data, make lists of all of the converted values (maybe a dictionary of time and voltage instead?)
    #   and store those data lists/dictionaries in a larger list 
    # So there should be two lists; one full of the headers and one full of the data
    # So HeaderList[0] would be the dictionary for the first header
    # and DataList[0] would be all of the data for the first header
    # and so on for each measurement
    #***TODO*** -> If a filter is present, include that in yet another list
    
    #  Setup Necessary Lists, Call Functions for Dictionaries
    #    The nice, final lists that will contain the end results and dictionaries and such
    HeaderList = []
    DataList = []
    if(FilterPresent == 1):
        FilterList = []
    #    The first set of lists that will contain the tuples from reading in the binary data
    #HeaderTupleList = []
    #DataTupleList = []
    #    The second set of lists that will contain the integer values after fixing the tuples from the first set
    #HeaderIntList = []
    #DataIntList = []
    
    #Read in Blocks of Data and Headers
    #  Format is 100 header bytes followed by 2000000 bytes of data
    #  So need to read in 100 bytes, store that in a temporary header list
    #  then read in 2000000 bytes of data, store that in a temporary data list
    #  Repeat this cycle until out of header - need to determine the number of headers
    #  Need to convert from Tuples to more useable list, also temporary
    # Since dealing with a lot of data, need to make sure to delete lists once they've served their purpose
    
    #    Need to find the number of headers in the file:
        #***TODO*** -> Find out if this method for length finding is valid for 2.0 as well
    if(FilterPresent == 0):
        #if no filter is present, as found earlier
        NumberTriggers = int(FileLength/2000100)
    elif(FilterPresent == 1):
        #if a filter is present, as found earlier
        NumberTriggers = int(FileLength/2000118)
    
    #Define Number of Header and Data Bytes
    NumberHeaderBytes=100
    NumberFilterBytes=18
    NumberDataBytes=2000000
    
    # With number of samples known, read in headers and data
    #***TODO*** -> Find a much more memory efficient way of doing this... crashed computer running all the samples at once
    #              Takes 4-5 gigs of RAM to execute and store data - make sure you have that much free...
    #              Also, don't run more than 50 sample files, that really starts pushing it...
    #***Need to just take care of everything in one loop, one sample at a time to save memory use
    for DataSet in range(NumberTriggers):
        #Define Temporary Lists to store a single header and data set, will get cleared each sample
        #  Tuple lists that will be used to read in data
        HdrReadList = []
        DataReadList = []
        if(FilterPresent == 1):
            FilterReadList = []
        #  Integer lists that will be used to manipulate data into its final form, also cleared each sample
        HdrIntList = []
        DataIntList = []
        if(FilterPresent == 1):
            FilterIntList = []
        
        #Read in one header and one set of data if no filter present; 
        #    if filter is present, it will be read after header in the proper order
        for Hdrs in range(NumberHeaderBytes):
            HdrReadList.append(struct.unpack("B", SensorFile.read(1)))
        if(FilterPresent == 1):
            for Filters in range(NumberFilterBytes):
                FilterReadList.append(struct.unpack("B", SensorFile.read(1)))  
        for Sample in range(int(NumberDataBytes/2)):
            DataReadList.append(struct.unpack("h", SensorFile.read(2)))
        
            
        #Now have lists of tuples, one for the header and one for the data (and one for filters)
        #Need to convert these lists of tuples into lists of integers (otherwise can't really use them...)
        for i in range(NumberHeaderBytes):
            HdrIntList.append(int(HdrReadList[i][0]))   
        for x in range(int(NumberDataBytes/2)):
            DataIntList.append(int(DataReadList[x][0]))
        if(FilterPresent == 1):
            for y in range(NumberFilterBytes):
                FilterIntList.append(int(FilterReadList[y][0]))
            
        #Now have an integer list of the 100 header bytes (HdrIntList) and an integer list of the 1000000 data 2-byte samples (DataIntList)
        #    and maybe an 18 byte integer list for filters
        #Clear out "Read" Lists for Memory
        HdrReadList = []
        DataReadList = []
        if(FilterPresent == 1):
            FilterReadList = []
        
        #With integer lists, can now convert into actual header information and data values
        #  Send Header Bytes to Header Function to translate header bytes into a dicitonary
        #  Get header Dictionary Back and Store in Final HeaderList to return to user
        #Check Version and send to proper header function
        #***TODO*** -> Add in other HAMMA versions
        #HAMMA 1.5
        if(Version == 'HAMMA_15'):
            HdrDict = hamma_convert_header_15(HdrIntList)
        #HAMMA 2.0
        elif(Version == 'HAMMA_20'):
            HdrDict = hamma_convert_header_20(HdrIntList)
        #If the HAMMA version is not recognized or not able to be handled yet
        else:
            print('Not a recognized HAMMA version...')
            return
        #Add dictionary to Final Header List to be returned to user
        HeaderList.append(HdrDict)
        #Clear remaining Header lists and such no longer needed (AKA free up some memory)
        HdrIntList = []
        
        #  Send Data Bytes to the Data Function to translate data into voltages (and times?)
        #  Get data back as either lists or dictionary, which will be stored in DataList and returned to user
        #Send Data to function for conversion
        DataDict = hamma_data_converter(DataIntList)
        #Add in dictionary to list to be return to user
        DataList.append(DataDict)
        #Clear out remaining data lists
        DataIntList = []      
        
        
        #  If a filter is present, translate that into its components and return to user
        if(FilterPresent == 1):
            #Send Filter bytes to filter function
            #***TODO*** -> find out filter structure definitions and make actual filter results
            FilterDict = hamma_filter_converter(FilterIntList)
            #Store Filter dictionary in filter list
            FilterList.append(FilterDict)
            #Clear out remaining filter lists
            FilterIntList = []
        
        
    #Return Results to User
    #  Debug/testing return statements
    #return FileLength, FilterPresent, SyncByte
    #return HdrReadList, DataReadList, HdrIntList, DataIntList
    #  Actual return statement
    if(FilterPresent == 1):
        return HeaderList, DataList, FilterList
    else:
        return HeaderList, DataList
#END


#Function for checking HAMMA version
def hamma_version_check(SyncByte):
    #HAMMA 1.5
    if(SyncByte == 242): #242 = 'F2'
        #Then this is HAMMA 1.5 and we'll use that header definition, stored in a seperate helper function for clarity
        Version = 'HAMMA_15'
    #HAMMA 2.0
    elif(SyncByte == 245):
        Version = 'HAMMA_20'    
    #HAMMA 1.0
    elif(SyncByte == 241):
        Version = 'HAMMA_10'
    #Error thrower in case version is not recognized
    else:
        #If not a recognized version, throw "error"
        print('The file does not seem to be a HAMMA version this program can handle...')
        return
    
    return Version
#END


#Define HAMMA Header Functions
#  HAMMA v1.5
def hamma_convert_header_15(HeaderIntList):
    """This code, given 100 header bytes, converts those bytes into a 
    dictionary of actual header variables and values"""
    #***TODO*** -> Make bytes to integer code more modular?
    #Define Empty Dictionary
    HdrDict = {}
    
    #Bytes 0-7: Sync Bytes
    #  Get Hex Values for Each Byte and Add to Text String
    SyncValue = ''
    for SyncByte in range(8):
        HexValue = '%02X'%HeaderIntList[SyncByte]
        HexString = str(HexValue)
        SyncValue += HexString
    HdrDict['SyncValue'] = SyncValue
    
    #Byte 8: Board ID
    HdrDict['BoardID'] = HeaderIntList[8]
    
    #Byte 9: Firmware Version
    HdrDict['FirmwareVersion'] = HeaderIntList[9]
    
    #Bytes 10-13: Payload Data Size
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 10, HeaderIntList)
    #With Byte Array, conver four bytes into a single integer tuple
    DataSize = struct.unpack('L', bytestr) #**Controlling this here to have more direct control over whether signed or not
    #Convert from tuple to integer
    DataSize = DataSize[0]
    #store integer value in dictionary
    HdrDict['DataSize'] = DataSize
    
    #Byte 14: Number of Channels
    HdrDict['NumChannels'] = HeaderIntList[14]
    
    #Bytes 15-16: Temperature 1
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 15, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Temp1 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Temp1 = Temp1[0]
    #Store Integer value in dictionary
    HdrDict['Temp1'] = Temp1
    
    #Bytes 17-18: Temperature 2
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 17, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Temp2 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Temp2 = Temp2[0]
    #Store Integer value in dictionary
    HdrDict['Temp2'] = Temp2
    
    #Bytes 19-20: WatchDog Timer
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 19, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Watchdog = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Watchdog = Watchdog[0]
    #Store Integer value in dictionary
    HdrDict['Watchdog'] = Watchdog
    
    #Byte 21: Fifo Overflow Counter
    HdrDict['FifoOverflow'] = HeaderIntList[21]
    
    #Byte 22: Padding Byte
    #******
    
    #Bytes 23-26: Trigger Position
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 23, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    TriggerPos = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    TriggerPos = TriggerPos[0]
    #Store Integer value in dictionary
    HdrDict['TriggerPos'] = TriggerPos
    
    #Bytes 27-30: Retrigger Enable Position
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 27, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    RetriggerENpos = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    RetriggerENpos = RetriggerENpos[0]
    #Store Integer value in dictionary
    HdrDict['RetriggerENpos'] = RetriggerENpos
    
    #Bytes 31-47: Padding Bytes
    #******
    
    #Bytes 48-55: GPS latitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 48, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Lat = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Lat = Lat[0]
    #Store Integer value in dictionary
    HdrDict['Lat'] = Lat
    
    #Bytes 56-63: GPS Longitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 56, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Lon = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Lon = Lon[0]
    #Store Integer value in dictionary
    HdrDict['Lon'] = Lon
    
    #Bytes 64-71: GPS Altitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 64, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Alt = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Alt = Alt[0]
    #Store Integer value in dictionary
    HdrDict['Alt'] = Alt
    
    #Bytes 72-75: GPS Time of Week
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 72, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    TimeOfWeek = struct.unpack('f', bytestr)
    #Convert from tuple to integer
    TimeOfWeek = TimeOfWeek[0]
    #Store Integer value in dictionary
    HdrDict['TimeOfWeek'] = TimeOfWeek
    
    #Bytes 76-77: GPS Week
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 76, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Week = struct.unpack('h', bytestr)
    #Convert from tuple to integer
    Week = Week[0]
    #Store Integer value in dictionary
    HdrDict['Week'] = Week
    
    #Bytes 78-81: GPS UTC Offset
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 78, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    UTCoffset = struct.unpack('f', bytestr)
    #Convert from tuple to integer
    UTCoffset = UTCoffset[0]
    #Store Integer value in dictionary
    HdrDict['UTCoffset'] = UTCoffset
    
    #Bytes 82-85: GPS Status
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 82, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSstatus = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSstatus = GPSstatus[0]
    #Store Integer value in dictionary
    HdrDict['GPSstatus'] = GPSstatus
    
    #Bytes 86-89: GPS Subsecond
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 86, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSsubsecond = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSsubsecond = GPSsubsecond[0]
    #Store Integer value in dictionary
    HdrDict['GPSsubsecond'] = GPSsubsecond
    
    #Bytes 90-93: GPS Subsecond ECC
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 90, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSsubsecECC = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSsubsecECC = GPSsubsecECC[0]
    #Store Integer value in dictionary
    HdrDict['GPSsubsecECC'] = GPSsubsecECC
    
    #Bytes 94-97: Time Tag Position (or Padding?)
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 94, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    TimeTagPos = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    TimeTagPos = TimeTagPos[0]
    #Store Integer value in dictionary
    HdrDict['TimeTagPos'] = TimeTagPos
    
    #Bytes 98-99: Packet Sequence
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 98, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    PacketSeq = struct.unpack('h', bytestr)
    #Convert from tuple to integer
    PacketSeq = PacketSeq[0]
    #Store Integer value in dictionary
    HdrDict['PacketSeq'] = PacketSeq
    
    #Return Dictionary of Header Information Back to main function to be store in HeaderList
    return HdrDict
#END

#  HAMMA v2.0
def hamma_convert_header_20(HeaderIntList):
    """This code, given 100 header bytes, converts those bytes into a 
    dictionary of actual header variables and values"""
    #***TODO*** -> Make bytes to integer code more modular?
    #Define Empty Dictionary
    HdrDict = {}
    
    #Bytes 0-7: Sync Bytes
    #  Get Hex Values for Each Byte and Add to Text String
    SyncValue = ''
    for SyncByte in range(8):
        HexValue = '%02X'%HeaderIntList[SyncByte]
        HexString = str(HexValue)
        SyncValue += HexString
    HdrDict['SyncValue'] = SyncValue
    
    #Byte 8: Board ID
    HdrDict['BoardID'] = HeaderIntList[8]
    
    #Byte 9: Firmware Version
    HdrDict['FirmwareVersion'] = HeaderIntList[9]
    
    #Bytes 10-13: Payload Data Size
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 10, HeaderIntList)
    #With Byte Array, conver four bytes into a single integer tuple
    DataSize = struct.unpack('L', bytestr) #**Controlling this here to have more direct control over whether signed or not
    #Convert from tuple to integer
    DataSize = DataSize[0]
    #store integer value in dictionary
    HdrDict['DataSize'] = DataSize
    
    #Byte 14: Number of Channels
    HdrDict['NumChannels'] = HeaderIntList[14]
    
    #Byte 15: Filter/Padding?
    #***HdrDict['Padding'] = HeaderIntList[15]
    
    #Bytes 16-17: Temperature 1
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 16, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Temp1 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Temp1 = Temp1[0]
    #Store Integer value in dictionary
    HdrDict['Temp1'] = Temp1
    
    #Bytes 18-19: Temperature 2/Humidity
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 18, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Temp2 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Temp2 = Temp2[0]
    #Store Integer value in dictionary
    HdrDict['Humidity'] = Temp2
    
    #Bytes 20-21: WatchDog Timer
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 20, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Watchdog = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    Watchdog = Watchdog[0]
    #Store Integer value in dictionary
    HdrDict['Watchdog'] = Watchdog
    
    #Byte 22: Fifo Overflow Counter
    HdrDict['FifoOverflow'] = HeaderIntList[22]
    
    #Byte 23: Padding Byte
    #******
    
    #Bytes 24-25: Threshhold 1?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 24, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh1 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh1 = thresh1[0]
    #Store Integer value in dictionary
    HdrDict['thresh1'] = thresh1
    
    #Bytes 26-27: Threshhold 2?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 26, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh2 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh2 = thresh2[0]
    #Store Integer value in dictionary
    HdrDict['thresh2'] = thresh2
    
    #Bytes 28-29: Threshhold 3?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 28, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh3 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh3 = thresh3[0]
    #Store Integer value in dictionary
    HdrDict['thresh3'] = thresh3
    
    #Bytes 30-31: Threshhold 4?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 30, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh4 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh4 = thresh4[0]
    #Store Integer value in dictionary
    HdrDict['thresh4'] = thresh4
    
    #Bytes 32-33: Threshhold 5?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 32, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh5 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh5 = thresh5[0]
    #Store Integer value in dictionary
    HdrDict['thresh5'] = thresh5
    
    #Bytes 34-35: Threshhold 6?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 34, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh6 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh6 = thresh6[0]
    #Store Integer value in dictionary
    HdrDict['thresh6'] = thresh6
    
    #Bytes 36-37: Threshhold 7?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 36, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh7 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh7 = thresh7[0]
    #Store Integer value in dictionary
    HdrDict['thresh7'] = thresh7
    
    #Bytes 38-39: Threshhold 8?
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 38, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    thresh8 = struct.unpack('H', bytestr)
    #Convert from tuple to integer
    thresh8 = thresh8[0]
    #Store Integer value in dictionary
    HdrDict['thresh8'] = thresh8
    
    #Bytes 23-26: Trigger Position
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 23, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    TriggerPos = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    TriggerPos = TriggerPos[0]
    #Store Integer value in dictionary
    HdrDict['TriggerPos'] = TriggerPos
    
    #Bytes 27-30: Retrigger Enable Position
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 27, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    RetriggerENpos = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    RetriggerENpos = RetriggerENpos[0]
    #Store Integer value in dictionary
    HdrDict['RetriggerENpos'] = RetriggerENpos
    
    #Bytes 48-55: GPS latitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 48, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Lat = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Lat = Lat[0]
    #Store Integer value in dictionary
    HdrDict['Lat'] = Lat
    
    #Bytes 56-63: GPS Longitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 56, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Lon = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Lon = Lon[0]
    #Store Integer value in dictionary
    HdrDict['Lon'] = Lon
    
    #Bytes 64-71: GPS Altitude
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(8, 64, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Alt = struct.unpack('d', bytestr)
    #Convert from tuple to integer
    Alt = Alt[0]
    #Store Integer value in dictionary
    HdrDict['Alt'] = Alt
    
    #Bytes 72-75: GPS Time of Week
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 72, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    TimeOfWeek = struct.unpack('f', bytestr)
    #Convert from tuple to integer
    TimeOfWeek = TimeOfWeek[0]
    #Store Integer value in dictionary
    HdrDict['TimeOfWeek'] = TimeOfWeek
    
    #Bytes 76-77: GPS Week
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(2, 76, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    Week = struct.unpack('h', bytestr)
    #Convert from tuple to integer
    Week = Week[0]
    #Store Integer value in dictionary
    HdrDict['Week'] = Week
    
    #Bytes 78-81: GPS UTC Offset
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 78, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    UTCoffset = struct.unpack('f', bytestr)
    #Convert from tuple to integer
    UTCoffset = UTCoffset[0]
    #Store Integer value in dictionary
    HdrDict['UTCoffset'] = UTCoffset
    
    #Bytes 82-85: GPS Status
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 82, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSstatus = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSstatus = GPSstatus[0]
    #Store Integer value in dictionary
    HdrDict['GPSstatus'] = GPSstatus
    
    #Bytes 86-89: GPS Subsecond
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 86, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSsubsecond = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSsubsecond = GPSsubsecond[0]
    #Store Integer value in dictionary
    HdrDict['GPSsubsecond'] = GPSsubsecond
    
    #Bytes 90-93: GPS Subsecond ECC
    #Fetch Byte array for variable
    bytestr = hamma_header_read_bytes(4, 90, HeaderIntList)
    #With Byte Array, convert bytes into a single integer tuple
    GPSsubsecECC = struct.unpack('I', bytestr)
    #Convert from tuple to integer
    GPSsubsecECC = GPSsubsecECC[0]
    #Store Integer value in dictionary
    HdrDict['GPSsubsecECC'] = GPSsubsecECC
    
    #***TODO*** -> Find out if there are any additional bytes;
    #              If so, add them here. Also, figure out what the
    #              16 byte byte-array is doing here in the structure definition
    #              on BitBucket; is the 2.0 header 110 bytes long with filter
    #              filter included? Not sure...
    
    
    #Return Dictionary of Header Information Back to main function to be store in HeaderList
    return HdrDict
#END







#Define helper function for hamma_header_15 or other header readers
def hamma_header_read_bytes(NumBytes, StartByte, HeaderIntList):
    """Simple helper routine to read in array of bytes from header, 
    given desired number of bytes to be read and when the bytearray starts."""
    #Create Empty Byte Array
    bytestr = bytearray(0)
    #Read in integer for each byte from HeaderIntList and convert back to byte value
    #  Combine these bytes into a single string of bytes and store in bytearray
    for x in range(NumBytes):
        y = HeaderIntList[x+StartByte].to_bytes(1, byteorder='big')
        bytestr += y
    #return converted bytes to user 
    return bytestr
#END
    
    
#Define Data Function
def hamma_data_converter(DataIntList):
    "Routine that takes in 1000000 data samples and converts it into proper times and voltages"
    #Define empty dictionary
    DataDict = {}
    #Define Empty Voltage and Time Lists
    Voltages = []
    Time = []
    
    #Find length of data
    DataLength = len(DataIntList)
    #Define Initial Time
    #***TODO*** -> make this initial time changeable by keyword, so the user can
    #              select absolute, UTC, or local time and such instead of relative time
    InitialTime = 0
    
    #Find Conversion ratio from integer data to voltage
    #  HAMMA operates a 16 bit, +/-10V ADC
    DataToVolts = 10/32767
    #Translate Integer Data into Voltages, store in Voltages List
    for sample in range(DataLength):
        Voltages.append(DataIntList[sample]*DataToVolts)
    #Store Voltage in Dictionary to Return to User
    DataDict['Voltages'] = Voltages
    
    #Find Time of Each sample -> Need Trigger time?
    #***TODO*** -> make optional time keywords available (default here is relative time; need to add in UTC, local, etc)
    #              have other time options/information available via the header file
    #   At the moment, will default to relative time to start of data
    #   Since the data is one second long there are exactly 1000000 samples, each sample is a microsecond since the previous
    #   This makes finding the relative time very easy... and feels like a waste of memory
    for sample in range(DataLength):
        RelativeTime = InitialTime+sample
        Time.append(RelativeTime)
    
    #Store Time List in Dictionary to Return to the User
    DataDict['Time'] = Time
    
    #Clear Leftover Lists that won't get used anymore
    Voltages = []
    Time = []
    
    return DataDict
#END



#Define Filter Function
def hamma_filter_converter(FilterIntList):
    "Routine to translate out filter information... Needs to be finished."
    #Define Empty Dictionary
    FilterDict = {}
    
    #Filter Structure:
    
    
    
    
    return FilterDict
#END



#Define a nicer default plotting option
def hamma_plot(DataList, TriggerNumber, Title='HAMMA Data', Start=0, Finish=1000000):
    """Given a trigger's data and position in file (what # trigger is it), 
    plots HAMMA data (defaults to whole waveform, but user can change it using start/finish)"""
    #Given the DataList List of Dictionaries and the Index (TriggerNumber) you want to graph,
    #Creates a lovely chart for the user, similar in style to HUDAT
    #User can optionally provide title, defaults to 'HAMMA Data'
    #User can also optionally input range of data they wish to plot using
    #  "start" and "finish" keyowrds, where Start is the first index they wish to see
    #   and Finish is one more than the last index they wish to see
    #Make sure that there are no "None" objects, set them = 0
    
    
    #SetFigure Size
    fig = plt.figure(figsize=(10, 2)) #Creates a 10in by 2in plot at 80 dpi(Default)
    #plt.rcParams["figure.figsize"] = (10,2)
    
    #First, Plot Data (in order x, y or time, voltages from Dictionary in DataList[TriggerNumber])
    plt.plot(DataList[TriggerNumber]['Time'][Start:Finish], 
             DataList[TriggerNumber]['Voltages'][Start:Finish],
             linewidth=0.75)
    
    #With Graph Up, Apply Plot Options
    #  Title
    plt.title(Title)
    #  Label Axes
    #***TODO*** -> Make the time label based on the units of time given. Here, defaults to microseconds...
    plt.xlabel('Time (us)')
    plt.ylabel('Voltage (V)')
    
    #Display Plot to Screen
    plt.show(block=False)
    return plt
#END


#Function to find the "offset" of the e-field data
def hamma_offset(DataList, TriggerNumber, NumSamples=1000, StartOfAvg=0):
    "Given the full data set w/ desired trigger index, find the offset of the e-field (avg of first # samples)"
    #Two modes? One that finds initial offset and one that finds end offset?
    #Default to 0, start, or can enter a value for StartOfAvg to start later in sample set 
    #                           (typically >800,000 for end of trigger)
    #Also, probably want standard deviation too
    
    #Initialize sum
    Sum = 0
    
    #Find sum of given number of samples in DataList
    for sample in range(NumSamples):
        Sum = Sum+(DataList[TriggerNumber]['Voltages'][StartOfAvg+sample])
    
    #Find average and store as Offset
    Offset = Sum/NumSamples
    
    #Find Standard Deviation
    #Start by finding difference from mean
    Diff = []
    
    for sample in range(NumSamples):
        Diff.append(DataList[TriggerNumber]['Voltages'][StartOfAvg+sample]-Offset)
    
    #Find Average of this Difference
    StdDevSum = 0
    
    for sample in range(NumSamples):
        StdDevSum = StdDevSum+Diff[sample]
    
    StdDevAvg = StdDevSum/NumSamples
    
    #Find standard deviation
    StdDev = math.sqrt(StdDevAvg)
    
    #Return average/offset and standard deviation to the user
    return Offset, StdDev
#END


#Function to see if the rails of measurement were hit for any extended period of time
def hamma_diagnostics_rails(DataList, TriggerNumber, SequenceNum=3, RailVal = 10):
    """Intakes the dataset and trigger and tells the user if and when
       the sensor rails were hit for an extended period of time.
       Useful for determining if gain needs to be changed.
       
       Returns a list of dictionaries containing when a sequence of rail hitting occured
       and how many samples were involved.
       
       Defaults to sequences of 3 rail samples, but can change via keyword SequenceNum .
       
       Also, rail values default to +/- 10V but this can be changed with
        keyword RailVal to +/- whatever value you want."""
       
    #Define empty list for storing indices of when the rails were hit
    RailInd = []
    #Define List to Hold Dictionaries of Bad Indices, 
    #  or indices where the rails are hit for a given period of time
    BadInd = []
    
    #Find number of samples
    NumSamples = int(len(DataList[TriggerNumber]['Voltages']))
        
    #Check to see if rails were reached (+/-10V) and record the indices
    for sample in range(NumSamples):
        if(DataList[TriggerNumber]['Voltages'][sample] >= RailVal or 
           DataList[TriggerNumber]['Voltages'][sample] <= -RailVal):
            #If rail was exceeded or matched, append index to Rail index list
            RailInd.append(sample)
    
    #Find length of RailInd
    NumInd = int(len(RailInd))
    
    #check to see if any samples actually did hit the rails; if not, tell user and stop here
    if(NumInd == 0):
        print('There were no values that reached the rails...')
        return BadInd
    
    #Check to see if the rails were exceeded for any lengthy period of time,
    #   defined to be more than 3 (default) samples in a row.
    #Mark the starting sample and count in another list
    #  Set initial check value to first index of RailInd
    PrevSample = RailInd[0]
    count=1
    #   Check to see if there are 3 consecutive samples in RailInd, mark first bad index
    for sample in range(NumInd):
        #Check to see if index is one greater than the previous one (they are consecutive)
        if(RailInd[sample] == PrevSample+1):
            count += 1
        #If value is no longer consecutive
        else:
            #Check to see if there were instances with consecutive enough rail samples
            if(count >= SequenceNum):
                #Add info to list of when the rails were consecutively hit
                hamma_diagnostics_rails_helper(PrevSample, count, BadInd)
            #Reset any posible count to 0
            count = 1
            #Do nothing if there is nothing to note
        PrevSample = RailInd[sample]
    
    #Extra check for end of list
    #If there are consecutive samples up to the end of the RailInd list, then there is no final
    #"different" sample to tell it to record the information of the final consecutive series
    #Need some final check that at the end of RailInd to ensure
    #that this last series of rail values is stored
    if(sample == (NumInd-1)):
        #Check to see if there were instances with consecutive enough rail samples
        if(count >= SequenceNum):
            #Add info to list of when the rails were consecutively hit
            hamma_diagnostics_rails_helper(PrevSample, count, BadInd)
    
    
    #Return results to the user
    return BadInd
    
#END

def hamma_diagnostics_rails_helper(PrevSample, count, BadInd):
    """"Creates dictionary of index of the first sample of the consecutive rail samples and 
        the number of samples and appends that dictionary to list of bad sample dictionaries. 
        Done here to not overwrite old dictionary values"""
    #Create Dictionary for rail info
    BadInfo = {}
    #Store starting index of rail collision and number at rail in dictionary
    BadInfo['StartingIndex'] = PrevSample-(count-1)
    BadInfo['NumberRailSamples'] = count
    #Add dictionary to list to be returned to user
    BadInd.append(BadInfo)
#END


#Leftover comments from early planning stages of code

#Define Functions
#	Read in Binary HAMMA Data and Convert into Decimal

#HAMMA Byte Definitions:
#Setup Cases for HAMMA 1.5 and 2.0; allow user to switch between the two with keywords
#
#




#Essential Data Process
# 1) Read in HAMMA Data File
# 2) Note which system the data is from given the  sync bytes
# 3) Using the byte definitions for that sensor model, store the data in a structure
# 4a) Read in set of 100 bytes, do bit math on 100 byte sample to append each of the necessary structures by the given values by sample
# 4b) ORRRRR... cyclically read in the bytes for each part of structures, repeating every 100 bytes
    # so read in first part (say 2 bytes), then next part (say 3 bytes), and so on until the sample is over, then restart
# 4c) Read in every say part of the byte structure that goes in one type of variable, then the next, and so on...
# 5) eventually have keywords to skip storing certain aspects of the data?
